## Завантаження файлу 

`POST : localhost:8080/api/employer/uploadFile`

## Пошук публічних діячів за ПІБ
`GET : localhost:8080/api/employer/getPublicFigure_Ua`

`GET : localhost:8080/api/employer/getPublicFigure_En`

## Топ 10 імен серед публічних діячів 

`GET : localhost:8080/api/employer/getListPopularPerson`