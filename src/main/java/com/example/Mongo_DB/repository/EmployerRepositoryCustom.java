package com.example.Mongo_DB.repository;

import com.example.Mongo_DB.data.EmployerData;
import com.example.Mongo_DB.dto.EmployerPopularNameDto;
import com.example.Mongo_DB.dto.EmployerQueryDto;

import java.util.List;

public interface EmployerRepositoryCustom {
    List<EmployerData> findByNameEN(EmployerQueryDto name);

    List<EmployerData> findByNameUA(EmployerQueryDto name);

    List<EmployerPopularNameDto> getListPopularPerson();
}
