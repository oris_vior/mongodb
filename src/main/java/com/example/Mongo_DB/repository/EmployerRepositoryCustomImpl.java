package com.example.Mongo_DB.repository;

import com.example.Mongo_DB.data.EmployerData;
import com.example.Mongo_DB.dto.EmployerPopularNameDto;
import com.example.Mongo_DB.dto.EmployerQueryDto;
import com.mongodb.client.MongoCollection;
import lombok.RequiredArgsConstructor;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.LinkedList;
import java.util.List;

import static com.mongodb.client.model.Accumulators.sum;
import static com.mongodb.client.model.Aggregates.*;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Sorts.descending;
import static java.util.Arrays.asList;

@Repository
@RequiredArgsConstructor
public class EmployerRepositoryCustomImpl implements EmployerRepositoryCustom {
    private final MongoTemplate mongoTemplate;

    @Override
    public List<EmployerData> findByNameEN(EmployerQueryDto queryDto) {
        Query mongoQuery = new Query();
        mongoQuery.addCriteria(Criteria.where("fullNameEn").regex(queryDto.getFirstNameEn())
                //Якщо нема збігу по призвищу, то шукаємо за ім`ям
                .orOperator(Criteria.where("firstNameEn").is(queryDto.getFirstNameEn().split(" ")[0])));
        return mongoTemplate.find(mongoQuery, EmployerData.class);
    }

    @Override
    public List<EmployerData> findByNameUA(EmployerQueryDto queryDto) {
        Query mongoQuery = new Query();
        mongoQuery.addCriteria(Criteria.where("fullName").regex(queryDto.getFirstNameEn())
                //Якщо нема збігу по призвищу, то шукаємо за ім`ям
                .orOperator(Criteria.where("firstName").is(queryDto.getFirstNameEn().split(" ")[0])));
        return mongoTemplate.find(mongoQuery, EmployerData.class);
    }

    @Override
    public List<EmployerPopularNameDto> getListPopularPerson() {
        MongoCollection<Document> collection = mongoTemplate.getCollection("employer");
        List<Bson> pipeline = asList(match(eq("isPep", true)), group("$firstNameEn", sum("count", 1)), sort(descending("count")), limit(10));
        List<EmployerPopularNameDto> list = new LinkedList<>();
        collection.aggregate(pipeline)
                .forEach(doc -> list.add(EmployerPopularNameDto.builder()
                        .name(doc.getString("_id"))
                        .count(doc.getInteger("count"))
                        .build()));
        return list;
    }
}

