package com.example.Mongo_DB.repository;

import com.example.Mongo_DB.data.EmployerData;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;


public interface EmployerRepository extends MongoRepository<EmployerData, String>, EmployerRepositoryCustom {
    @Override
    List<EmployerData> findAll();

    @Override
    <S extends EmployerData> List<S> saveAll(Iterable<S> entities);

    @Override
    void deleteAll();
}
