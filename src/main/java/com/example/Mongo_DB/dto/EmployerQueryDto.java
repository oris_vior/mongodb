package com.example.Mongo_DB.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

@Getter
@Setter
@FieldNameConstants
public class EmployerQueryDto {
    private String firstNameEn;
}
