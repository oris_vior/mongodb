package com.example.Mongo_DB.dto;

import com.example.Mongo_DB.data.Declaration;
import com.example.Mongo_DB.data.RelatedCompanies;
import com.example.Mongo_DB.data.RelatedPersons;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

@Getter
@Builder
@Jacksonized
public class EmployerInfoDto {

    private String id;
    private String lastJobTitle;
    private String firstNameEn;
    private String lastNameEn;
    private String fullNameEn;
    private String typeOfOfficial;
    private RelatedPersons[] relatedPersons;
    private RelatedCompanies[] relatedCompanies;
    private Boolean isPep;
    private String patronymic;
    private Declaration[] declarations;

}
