package com.example.Mongo_DB.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.jackson.Jacksonized;

@Getter
@Setter
@Jacksonized
@Builder
public class EmployerPopularNameDto {
    private String name;
    private Integer count;
}
