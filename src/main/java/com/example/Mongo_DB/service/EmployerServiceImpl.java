package com.example.Mongo_DB.service;

import com.example.Mongo_DB.data.EmployerData;
import com.example.Mongo_DB.dto.EmployerInfoDto;
import com.example.Mongo_DB.dto.EmployerPopularNameDto;
import com.example.Mongo_DB.dto.EmployerQueryDto;
import com.example.Mongo_DB.repository.EmployerRepository;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import net.lingala.zip4j.ZipFile;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class EmployerServiceImpl implements EmployerService {

    private final EmployerRepository employerRepository;

    @Override
    public List<EmployerInfoDto> getPersonFromNameEN(EmployerQueryDto name) {
        return employerRepository.findByNameEN(name).stream().map(this::toInfoDto).collect(Collectors.toList());
    }

    @Override
    public List<EmployerInfoDto> getPersonFromNameUA(EmployerQueryDto name) {
        return employerRepository.findByNameUA(name).stream().map(this::toInfoDto).collect(Collectors.toList());
    }

    @Override
    public List<EmployerPopularNameDto> getListPopularPerson() {
        return employerRepository.getListPopularPerson();
    }

    @SneakyThrows
    @Override
    public void saveFile(MultipartFile multipartFile, String name) {
        File fileZIP = convertMultipartFileToFile(multipartFile, name);
        List<EmployerData> dataList = new ArrayList<>();
        parseFileToList(fileZIP, dataList);
        if (!employerRepository.findAll().isEmpty()) {
            employerRepository.deleteAll();
        }
        employerRepository.saveAll(dataList);
    }

    private void parseFileToList(File fileZIP, List<EmployerData> dataList) throws IOException {
        ZipFile zipFile = new ZipFile(fileZIP);
        zipFile.extractAll("Extract");
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        JsonFactory factory = objectMapper.getFactory();
        JsonParser parser = factory.createParser(new File("Extract/pep.json"));
        while (!parser.isClosed()) {
            JsonToken token = parser.nextToken();
            if (JsonToken.START_OBJECT.equals(token)) {
                EmployerData employerData = objectMapper.readValue(parser, EmployerData.class);
                dataList.add(employerData);
            } else if (JsonToken.END_OBJECT.equals(token)) {
                // конец объекта
            } else if (JsonToken.START_ARRAY.equals(token)) {
                // начало массива
            } else if (JsonToken.END_ARRAY.equals(token)) {
                // конец массива
            } else if (JsonToken.FIELD_NAME.equals(token)) {
                token = parser.nextToken();
            }
        }
        parser.close();
    }

    @SneakyThrows
    private File convertMultipartFileToFile(MultipartFile multipartFile, String name) {
        File convFile = new File(name);
        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write(multipartFile.getBytes());
        fos.close();
        return convFile;
    }

    private EmployerInfoDto toInfoDto(EmployerData employerData) {
        return EmployerInfoDto.builder()
                .id(employerData.getId())
                .firstNameEn(employerData.getFirstNameEn())
                .lastJobTitle(employerData.getLastJobTitle())
                .lastNameEn(employerData.getLastNameEn())
                .fullNameEn(employerData.getFullNameEn())
                .typeOfOfficial(employerData.getTypeOfOfficial())
                .relatedPersons(employerData.getRelatedPersons())
                .relatedCompanies(employerData.getRelatedCompanies())
                .isPep(employerData.getIsPep())
                .patronymic(employerData.getPatronymic())
                .declarations(employerData.getDeclarations())
                .build();
    }
}
