package com.example.Mongo_DB.service;

import com.example.Mongo_DB.dto.EmployerInfoDto;
import com.example.Mongo_DB.dto.EmployerPopularNameDto;
import com.example.Mongo_DB.dto.EmployerQueryDto;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;


public interface EmployerService {

    void saveFile(MultipartFile file, String name);

    List<EmployerInfoDto> getPersonFromNameEN(EmployerQueryDto name);

    List<EmployerInfoDto> getPersonFromNameUA(EmployerQueryDto name);

    List<EmployerPopularNameDto> getListPopularPerson();
}
