package com.example.Mongo_DB.controller;

import com.example.Mongo_DB.dto.EmployerInfoDto;
import com.example.Mongo_DB.dto.EmployerPopularNameDto;
import com.example.Mongo_DB.dto.EmployerQueryDto;
import com.example.Mongo_DB.service.EmployerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/api/employer")
@RequiredArgsConstructor
public class EmployerController {

    private final EmployerService employerService;

    @PostMapping("/uploadFile")
    public ResponseEntity submit(@RequestParam MultipartFile file) {
        employerService.saveFile(file, file.getOriginalFilename());
        System.out.println("Well");
        return ResponseEntity.status(HttpStatus.OK)
                .body("file download to DB");
    }

    @GetMapping("/getPublicFigure_En")
    public List<EmployerInfoDto> getPersonEn(@RequestBody EmployerQueryDto query) {
        return employerService.getPersonFromNameEN(query);
    }

    @GetMapping("/getPublicFigure_Ua")
    public List<EmployerInfoDto> getPersonUa(@RequestBody EmployerQueryDto query) {
        return employerService.getPersonFromNameUA(query);
    }

    @GetMapping("/getListPopularPerson")
    public List<EmployerPopularNameDto> getListPopularPerson() {
        return employerService.getListPopularPerson();
    }
}
